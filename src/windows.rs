use crate::*;
use egui::{CtxRef, ScrollArea, Ui, Window};
use std::{
    collections::BTreeSet,
    //sync::{
    //    Arc, Mutex,
    //}
};

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize)]
#[serde(default)]
struct Demos {
    #[serde(skip)]
    demos: Vec<Box<dyn crate::Windows>>,

    open: BTreeSet<String>,
}

impl Default for Demos {
    fn default() -> Self {
        let demos: Vec<Box<dyn crate::Windows>> = vec![
            Box::new(crate::user_info::UserInfo::default()),
            Box::new(crate::painting::Painting::default()),
            Box::new(crate::tier_list::TierList::default()),
            Box::new(crate::todo_list::TodoList::default()),
        ];

        let mut open = BTreeSet::new();
        open.insert(crate::user_info::UserInfo::default().name().to_string());

        Self { open, demos }
    }
}
impl Demos {
    pub fn checkboxes(&mut self, ui: &mut Ui) {
        let Self { open, demos } = self;
        for demo in demos {
            let mut is_open = open.contains(demo.name());
            ui.checkbox(&mut is_open, demo.name());
            set_open(open, demo.name(), is_open);
        }
    }

    pub fn show(&mut self, ctx: &CtxRef) {
        let Self { open, demos } = self;
        for demo in demos {
            let mut is_open = open.contains(demo.name());
            demo.show(ctx, &mut is_open);
            set_open(open, demo.name(), is_open);
        }
    }
}

fn set_open(open: &mut BTreeSet<String>, key: &'static str, is_open: bool) {
    if is_open {
        if !open.contains(key) {
            open.insert(key.to_owned());
        }
    } else {
        open.remove(key);
    }
}

// ----------------------------------------------------------------------------

/// A menu bar in which you can select different demo windows to show.
#[derive(Default, Serialize, Deserialize)]
#[serde(default)]
pub struct DemoWindows {
    open_windows: OpenWindows,
    demos: Demos,
}

impl DemoWindows {
    /// Show the app ui (menu bar and windows).
    /// `sidebar_ui` can be used to optionally show some things in the sidebar
    pub fn ui(&mut self, ctx: &CtxRef) {
        egui::SidePanel::left("side_panel").show(ctx, |ui| {
            ui.heading("✒ Welcome!");

            ui.separator();

            ScrollArea::auto_sized().show(ui, |ui| {
                ui.heading("Windows:");
                self.demos.checkboxes(ui);

                ui.separator();

                ui.label("Debug:");
                self.open_windows.checkboxes(ui);

                ui.separator();

                if ui.button("Organize windows").clicked() {
                    ui.ctx().memory().reset_areas();
                }
            });
        });

        {
            let fill = ctx.style().visuals.extreme_bg_color;
            let frame = egui::Frame::none().fill(fill);
            egui::CentralPanel::default().frame(frame).show(ctx, |_| {});
        }

        self.windows(ctx);
    }

    /// Show the open windows.
    fn windows(&mut self, ctx: &CtxRef) {
        let Self {
            open_windows,
            demos,
            ..
        } = self;

        Window::new("🔧 Settings")
            .open(&mut open_windows.settings)
            .scroll(true)
            .show(ctx, |ui| {
                ctx.settings_ui(ui);
            });

        Window::new("🔍 Inspection")
            .open(&mut open_windows.inspection)
            .scroll(true)
            .show(ctx, |ui| {
                ctx.inspection_ui(ui);
            });

        Window::new("📝 Memory")
            .open(&mut open_windows.memory)
            .resizable(false)
            .show(ctx, |ui| {
                ctx.memory_ui(ui);
            });

        demos.show(ctx);
    }
}

// ----------------------------------------------------------------------------

#[derive(Serialize, Deserialize)]
struct OpenWindows {
    // egui stuff:
    settings: bool,
    inspection: bool,
    memory: bool,
}

impl Default for OpenWindows {
    fn default() -> Self {
        OpenWindows::none()
    }
}

impl OpenWindows {
    fn none() -> Self {
        Self {
            settings: false,
            inspection: false,
            memory: false,
        }
    }

    fn checkboxes(&mut self, ui: &mut Ui) {
        let Self {
            settings,
            inspection,
            memory,
        } = self;

        ui.checkbox(settings, "🔧 Settings");
        ui.checkbox(inspection, "🔍 Inspection");
        ui.checkbox(memory, "📝 Memory");
    }
}
