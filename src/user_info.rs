use crate::egui::*;

fn add_hyperlink(
    ui: &mut Ui,
    heading: impl Into<Label>,
    text: impl ToString,
    link: impl ToString,
) {
    ui.horizontal(|ui| {
        ui.label(heading);
        ui.add(Hyperlink::new(link).text(text));
    });
}

#[derive(Default)]
pub struct UserInfo {}

impl crate::Windows for UserInfo {
    fn name(&self) -> &'static str {
        "\u{2139} User Information"
    }

    fn show(&mut self, ctx: &CtxRef, open: &mut bool) {
        Window::new("FOSS Project: Robo Arc")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                ui.label("A discord bot with over 100 interactive commands!");
                ui.separator();
                add_hyperlink(ui, "Source Code:", "Available on gitlab under the MPL-2.0 licence!", "https://gitlab.com/vicky5124/robo-arc/");
                add_hyperlink(ui, "Bot invite link:", "Check the invite command for the reason behind each permission", "https://discord.com/api/oauth2/authorize?client_id=551759974905151548&scope=bot&permissions=808971478");
            });

        Window::new("FOSS Project: lavalink-rs")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                ui.label("A lavalink API wrapping library for every tokio.rs based discord crate.");
                ui.separator();
                add_hyperlink(ui, "Source Code:", "Available on gitlab under the MPL-2.0 licence!", "https://gitlab.com/vicky5124/lavalink-rs");
                add_hyperlink(ui, "Documentation", "Available on docs.rs", "https://docs.rs/lavalink-rs/");
                add_hyperlink(ui, "crates.io:", "here", "https://crates.io/crates/lavalink-rs");
                add_hyperlink(ui, "lib.rs:", "here", "https://lib.rs/crates/lavalink-rs");
            });

        Window::new("FOSS Project: lavasnek_rs")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                ui.label("A lavalink-rs wrapper for every python asyncio based discord library.");
                ui.separator();
                add_hyperlink(ui, "Source Code:", "Available on github under the MPL-2.0 licence!", "https://github.com/vicky5124/lavasnek_rs/");
                add_hyperlink(ui, "Documentation", "Available on github.io", "https://vicky5124.github.io/lavasnek_rs/lavasnek_rs/lavasnek_rs.html");
                add_hyperlink(ui, "pypi:", "here", "https://pypi.org/project/lavasnek-rs/");
            });

        Window::new("Files")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                ui.label("This is a list of publicly accessible files that are free to download (just random stuff)");
                add_hyperlink(ui, "Fancy indexer:", "here", "./HDD");
                add_hyperlink(ui, "Legacy indexer:", "here", "./old/HDD");
                ui.separator();
                add_hyperlink(ui, "Source of the file indexer", "ngx-superbindex", "https://github.com/gibatronic/ngx-superbindex");
                add_hyperlink(ui, "Source of this website", "Available on gitlab!", "https://gitlab.com/vicky5124/home-page");
            });

        Window::new("Social Media")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                add_hyperlink(ui, "Email:", "vickyf5124@gmail.com", "mailto:vickyf5124@gmail.com");
                add_hyperlink(ui, "TorMail:", "ssh74duw35joaisw.onion@dhosting", "mailto:ssh74duw35joaisw.onion@dhosting4xxoydyaivckq7tsmtgi4wfs3flpeyitekkmqwu4v4r46syd.onion");
                ui.separator();
                add_hyperlink(ui, "GitLab:", "vicky5124", "https://gitlab.com/vicky5124");
                add_hyperlink(ui, "GitHub:", "vicky5124", "https://github.com/vicky5124");
                ui.separator();
                add_hyperlink(ui, "Youtube:", "Victoria Casasampere Fernandez", "https://www.youtube.com/channel/UCmbaTh4b0dVK3MtiPojtY6g");
                add_hyperlink(ui, "Discord:", "vicky5124#2207", "https://discord.com");
                add_hyperlink(ui, "Twitter:", "TransfemLinux", "https://twitter.com/TransfemLinux");
                add_hyperlink(ui, "Twitch:", "vicky5124", "https://www.twitch.tv/vicky5124");
            });

        Window::new("Curriculum Vitae")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                add_hyperlink(ui, "English:", "PDF", "./HDD/Downloads/CV ENGLISH.pdf");
                add_hyperlink(ui, "Español:", "PDF", "./HDD/Downloads/CV SPANISH.pdf");
            });

        Window::new("Blogs and Posts")
            .open(open)
            .scroll(true)
            .resizable(true)
            .default_height(500.0)
            .default_width(10.0)
            .show(ctx, |ui| {
                add_hyperlink(ui, "MTF HRT Progression:", "here", "./hrt");
                add_hyperlink(ui, "Epilator Tips and Tricks:", "here", "./epilator");
            });
    }
}
