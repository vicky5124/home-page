#!/bin/sh

cargo build --release --target wasm32-unknown-unknown &&
wasm-bindgen "target/wasm32-unknown-unknown/release/home_page.wasm" --out-dir static --no-modules --no-typescript &&
wasm-opt static/home_page_bg.wasm -O3 --fast-math -o static/home_page_bg.wasm &&
#wasm-pack build --out-dir static --no-typescript
basic-http-server static -a "192.168.1.2:4000"
