#![warn(clippy::all)]

#[macro_use]
extern crate tracing;
#[macro_use]
extern crate serde;

mod painting;
mod tier_list;
mod todo_list;
mod user_info;
mod windows;

use eframe::wasm_bindgen::{self, prelude::*};
pub use eframe::{egui, epi};
use egui::*;

#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct HomePage {
    windows: windows::DemoWindows,
    //painting: painting::Painting,
}

impl Default for HomePage {
    fn default() -> Self {
        Self {
            windows: Default::default(),
        }
    }
}

impl epi::App for HomePage {
    fn name(&self) -> &str {
        "VickyPaint!"
    }

    fn setup(
        &mut self,
        _ctx: &CtxRef,
        _frame: &mut epi::Frame<'_>,
        storage: Option<&dyn epi::Storage>,
    ) {
        if let Some(s) = storage {
            *self = epi::get_value(s, epi::APP_KEY).unwrap_or_default();
        }
    }


    fn update(
        &mut self,
        ctx: &CtxRef,
        frame: &mut epi::Frame<'_>,
    ) {
        frame.set_window_size(Vec2::new(720.0, 720.0));

        self.windows.ui(ctx);
    }

    fn max_size_points(&self) -> Vec2 {
        Vec2::new(f32::INFINITY, f32::INFINITY)
    }

    fn save(&mut self, storage: &mut dyn epi::Storage) {
        epi::set_value(storage, epi::APP_KEY, self);
    }
}

pub trait Windows {
    // `&'static` so we can also use it as a key to store open/close state.
    fn name(&self) -> &'static str;

    // Show windows, etc
    fn show(&mut self, ctx: &CtxRef, open: &mut bool);
}

pub trait View {
    fn ui(&mut self, ui: &mut Ui);
}

#[wasm_bindgen]
pub fn start(canvas_id: &str) -> Result<(), eframe::wasm_bindgen::JsValue> {
    console_error_panic_hook::set_once();
    tracing_wasm::set_as_global_default();

    info!("Hello, Wasm!");

    let app = HomePage::default();
    eframe::start_web(canvas_id, Box::new(app))
}
