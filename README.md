# Home Page

To build the project just run the `build.sh` script and open statc/index.html on your browser!

A lot of this code is based off of the [demo of egui](https://github.com/emilk/egui/blob/master/egui_demo_lib/)
