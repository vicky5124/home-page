use crate::egui::*;
use std::fmt;

#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct TodoList {
    items: Vec<Item>,
    current_item: Item,
    show: Show,
}

#[derive(Default, Clone, Serialize, Deserialize)]
#[serde(default)]
struct Item {
    title: String,
    description: String,
    done: bool,
}

#[derive(PartialEq, Serialize, Deserialize)]
enum Show {
    All,
    Completed,
    NotCompleted,
}

impl fmt::Display for Show {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match self {
            Self::All => "All",
            Self::Completed => "Completed",
            Self::NotCompleted => "Not Completed",
        };
        write!(f, "{}", text)
    }
}

impl Default for TodoList {
    fn default() -> Self {
        Self {
            items: vec![],
            current_item: Item::default(),
            show: Show::All,
        }
    }
}

impl crate::Windows for TodoList {
    fn name(&self) -> &'static str {
        "📓 Todo List"
    }

    fn show(&mut self, ctx: &CtxRef, open: &mut bool) {
        use crate::View;
        Window::new(self.name())
            .open(open)
            .default_size(vec2(256.0, 256.0))
            .scroll(true)
            .resizable(true)
            .show(ctx, |ui| self.ui(ui));
    }
}

impl crate::View for TodoList {
    fn ui(&mut self, ui: &mut Ui) {
        ui.horizontal(|ui| {
            ui.label("Title:");
            ui.add(TextEdit::singleline(&mut self.current_item.title));
        });

        ui.horizontal(|ui| {
            ui.label("Description:");
            ui.add(TextEdit::singleline(
                &mut self.current_item.description,
            ));
            if ui.input().key_pressed(Key::Enter) && !self.current_item.title.is_empty() {
                self.items.push(self.current_item.clone());
                self.current_item = Item::default();
            }
        });

        ComboBox::from_label("Display Filters")
            .selected_text(self.show.to_string())
            .show_ui(ui, |ui| {
                ui.selectable_value(&mut self.show, Show::All, Show::All.to_string());
                ui.selectable_value(&mut self.show, Show::Completed, Show::Completed.to_string());
                ui.selectable_value(
                    &mut self.show,
                    Show::NotCompleted,
                    Show::NotCompleted.to_string(),
                );
            });

        ui.separator();

        ui.vertical(|ui| {
            for ref mut i in &mut self.items {
                match self.show {
                    Show::Completed if !i.done => continue,
                    Show::NotCompleted if i.done => continue,
                    _ => (),
                }

                ui.horizontal(|ui| {
                    let toggle = ui
                        .add(SelectableLabel::new(i.done, "✅"))
                        .on_hover_text("Mark as complete!");
                    if toggle.clicked() {
                        i.done = !i.done;
                    }

                    ui.label(&i.title);
                    ui.add(TextEdit::singleline(&mut i.description));
                });
            }
        });
    }
}
